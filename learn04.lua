----------------------------------------------------
-- 4. Modules.
----------------------------------------------------

--[[ I'm commenting out this section so the rest of
this script remains runnable.

-- Suppose the file mod.lua looks like this:

local M = {}

local function sayMyName()
    print('Hrunkner')
end

function M.sayHello()
    print('Why hello there')
    sayMyName()
end

return M



-- Another file can use mod.lua's functionality:
local mod = require('mod') -- run the file mod.lua.

-- require is the standard way to include modules.
-- require acts like: (if not cached; see below)
local mod = (function ()
    <contents of mod.lua>
end)()
-- It's like mod.lua is a function body, so that
-- locals inside mod.lua are invisible outside it.
-- This works because mod here = M in mod.lua:
mod.sayHello() -- Prints: Why hello there Hrunkner

-- This is wrong; sayMyName only exists in mod.lua:
mod.sayMyName() -- error

local a = require('mod2')
local b = require('mod2')

-- dofile is like require without caching:
dofile('mod2.lua') -- Hi!
dofile('mod2.lua') -- Hi! (runs it again)

-- loadfile loads a lua file but doesn't run it yet.
f = loadfile('mod2.lua') -- call f() to run it.

-- load is loadfile for strings.
-- (loadstring is deprecated, use load instead)
g = load('print(343)') -- Returns a function.
g() -- Prints out 343; nothing printed before now.
--]]
