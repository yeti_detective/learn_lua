# Yeti Detective Learns Lua
This is me doing the, uh... tutorial? from [Learn Lua in Y Minutes](https://learnxinyminutes.com/docs/lua/)

I kind of broke the sections of the lesson into their own discrete files so I could do it in the ["Learn the Hard Way"](https://learncodethehardway.org/) style

You can install Lua locally by [downloading the source code and building it](https://www.lua.org/download.html) (*what?*)

